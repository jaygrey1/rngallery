import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    // width: 50,
    height: 100,
    marginBottom: 10,

    backgroundColor: 'red',
  },
  imageView: {
    width: 100,
    height: 100,
    // backgroundColor: 'blue',
  },

  textView: {
    flexDirection: 'column',
    // width: 100,
    height: 100,
    flexGrow: 1,
    alignItems: 'stretch',

    backgroundColor: 'green',
  },

  authorContainer: {
    // width: 50,
    // height: 50,
    flexBasis: 1,
    flexGrow: 1,
    padding: 10,
    justifyContent: 'center',
    backgroundColor: 'orange',
  },

  authorText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
  },

  descriptionContainer: {
    // width: 50,
    // height: 50,
    flexGrow: 2,
    padding: 10,
    justifyContent: 'flex-start',
    backgroundColor: 'grey',
  },

  descriptionText: {
    fontSize: 18,
    textAlign: 'left',
    paddingRight: 100,
  },
});

export const ListItem = props => {
  const {
    item: { id, author, description, urls },
    onPress,
  } = props;
  const imageSource = { uri: urls.small };
  return (
    <View style={styles.container}>
      <View style={styles.imageView}>
        <TouchableOpacity onPress={() => onPress(id)}>
          <Image style={styles.imageView} source={imageSource} />
        </TouchableOpacity>
      </View>
      <View style={styles.textView}>
        <View style={styles.authorContainer}>
          <Text style={styles.authorText}>{author}</Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.descriptionText}>{description}</Text>
        </View>
      </View>
    </View>
  );
};
