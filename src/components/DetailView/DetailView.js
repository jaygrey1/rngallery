import React from 'react';

import { Image, View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  image: {
    flex: 1,
    alignSelf: 'stretch',
  },
});

export const DetailView = ({ item, onPress }) => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: item.urls.large }} />
    </View>
  );
};
