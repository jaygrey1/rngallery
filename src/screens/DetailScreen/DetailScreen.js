import React from 'react';

import DetailViewContainer from '../../containers/DetailViewContainer';

export const DetailsScreen = ({ navigation }) => {
  const photoId = navigation.getParam('id');
  return <DetailViewContainer id={photoId} onPress={() => navigation.navigate('Home')} />;
};
