import { createStackNavigator, createAppContainer } from 'react-navigation';

import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';

export const Navigation = createAppContainer(
  createStackNavigator(
    {
      Home: {
        screen: HomeScreen,
      },
      Detail: {
        screen: DetailScreen,
      },
    },
    {
      initialRouteName: 'Home',
      headerMode: 'none',
    }
  )
);
