import React from 'react';
import ListContainer from '../../containers/ListContainer';

export const HomeScreen = props => <ListContainer {...props} />;
