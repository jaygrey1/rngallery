import React from 'react';
import { connect } from 'react-redux';

import * as photosSelector from '../../store/selectors/photos';
import DetailView from '../../components/DetailView';

const DetailViewContainer = props => <DetailView {...props} />;

const mapStateToProps = (state, ownProps) => ({
  item: photosSelector.getPhoto(ownProps.id, state),
});

export default connect(mapStateToProps)(DetailViewContainer);
