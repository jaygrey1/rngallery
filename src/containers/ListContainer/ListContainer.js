import React, { useEffect } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';

import ListItem from '../../components/ListItem';

import * as photosSelector from '../../store/selectors/photos';
import * as photosAction from '../../store/actions/photos';

const ListContainer = ({ photos, isFetched, getPhotos, isLoading, currentPage, navigation }) => {
  useEffect(() => {
    if (!isFetched) {
      getPhotos();
    }
  });

  return (
    <FlatList
      data={photos}
      // style={{ flexDirection: 'column' }}
      onEndReachedThreshold={0.25}
      refreshing={isLoading}
      onEndReached={() => {
        getPhotos(currentPage);
      }}
      renderItem={props => (
        <ListItem
          key={props.item.id}
          {...props}
          onPress={() => {
            navigation.navigate('Detail', { id: props.item.id });
          }}
        />
      )}
    />
  );
};

const mapStateToProps = state => ({
  photos: photosSelector.getPhotos(state),
  isFetched: photosSelector.isFetched(state),
  isLoading: photosSelector.isLoading(state),
  currentPage: photosSelector.getCurrentPage(state),
});

const mapDispatchToProps = {
  getPhotos: photosAction.getPhotos,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListContainer);
