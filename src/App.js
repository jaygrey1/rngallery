/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import { Provider } from 'react-redux';

import { Navigation } from './screens/configureNavigator';
import store from './store/configureStore';

const App = () => (
  <Provider store={store}>
    <Navigation />
  </Provider>
);

export default App;
