export const getOrThrow = async url => {
  const result = await fetch(url, {
    method: 'get',
    headers: {
      Accept: 'application/json',
    },
  });

  if (!result.ok) {
    throw new Error('fetching error');
  }

  return result.json();
};
