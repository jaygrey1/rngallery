import { getOrThrow } from '../helpers/web';

const host = 'https://api.unsplash.com/';
const clientId = 'cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

export const getPhotos = async page => {
  const endpoint = 'photos';
  const result = await getOrThrow(`${host}${endpoint}?client_id=${clientId}&page=${page}`);

  return result.map(item => ({
    id: item.id,
    description: item.description || item.alt_description || '',
    author: item.user.username,
    urls: { small: item.urls.thumb, large: item.urls.small },
  }));
};
