import * as t from '../actionTypes';

const initialState = {
  photos: null,
  isLoading: false,
  currentPage: 1,
};

const photosReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.GET_PHOTOS_REQUEST: {
      return { ...state, isLoading: true };
    }

    case t.GET_PHOTOS_SUCCESS: {
      return {
        ...state,
        photos:
          state.currentPage > 1
            ? [
                ...state.photos,
                ...action.payload.filter(
                  item => state.photos.find(photo => photo.id === item.id) === undefined
                ),
              ]
            : action.payload,
        isLoading: false,
        currentPage: state.currentPage + 1,
      };
    }

    case t.GET_PHOTOS_FAILURE: {
      return { ...state, isLoading: false };
    }

    default: {
      return state;
    }
  }
};

export default photosReducer;
