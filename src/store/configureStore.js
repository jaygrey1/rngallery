import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
// import userReducer from './reducers/user';
import photosReducer from './reducers/photos';

export default createStore(photosReducer, applyMiddleware(thunk));
