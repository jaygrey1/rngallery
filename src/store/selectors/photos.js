export const getPhotos = state => {
  return state.photos;
};

export const isLoading = state => {
  return state.isLoading;
};

export const getCurrentPage = state => {
  return state.currentPage;
};

export const isFetched = state => {
  return state.photos !== null;
};

export const getPhoto = (id, state) => {
  return state.photos.find(item => item.id === id);
};
