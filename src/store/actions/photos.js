import * as t from '../actionTypes';

import * as photosService from '../../services/photos';

const getPhotosRequestAction = () => ({
  type: t.GET_PHOTOS_REQUEST,
});

const getPhotosSuccesAction = payload => ({
  type: t.GET_PHOTOS_SUCCESS,
  payload,
});

const getPhotosFailureAction = () => ({
  type: t.GET_PHOTOS_FAILURE,
});

export const getPhotos = (currentPage = 1) => async dispatch => {
  dispatch(getPhotosRequestAction);
  try {
    const result = await photosService.getPhotos(currentPage);
    dispatch(getPhotosSuccesAction(result));
  } catch (error) {
    dispatch(getPhotosFailureAction());
  }
};
